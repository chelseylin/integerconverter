"""
Web service for the Integer Converter program.
"""

import cherrypy
from integer_converter import IntegerConverter

HTTP_BAD_REQUEST = 400

cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 8080,
                       })

@cherrypy.expose
class IntegerConverterService(object):

    def GET(self, n=False):
        """
        Takes a parameter n and returns the converted string for the integer.
        """
        c = IntegerConverter()
        if not n:
            response = "Missing Input: Please provide the following data: 'n'."
            cherrypy.response.status = HTTP_BAD_REQUEST

        elif not c.input_validator(n):
            response = "Invalid Input: n must be non-negative integer less than 10&22."
            cherrypy.response.status = HTTP_BAD_REQUEST

        else:
            response = c.convert(n)

        return response

if __name__ == '__main__':
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    }
    cherrypy.quickstart(IntegerConverterService(), '/', conf)
