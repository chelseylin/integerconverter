NUM_LOOKUP_TABLE = {
    "1": "One",
    "2": "Two",
    "3": "Three",
    "4": "Four",
    "5": "Five",
    "6": "Six",
    "7": "Seven",
    "8": "Eight",
    "9": "Nine",
    "10": "Ten",
    "11": "Eleven",
    "12": "Twelve",
    "13": "Thirteen",
    "14": "Fourteen",
    "15": "Fifteen",
    "16": "Sixteen",
    "17": "Seventeen",
    "18": "Eighteen",
    "19": "Nineteen",
    "20": "Twenty",
    "30": "Thirty",
    "40": "Forty",
    "50": "Fifty",
    "60": "Sixty",
    "70": "Seventy",
    "80": "Eighty",
    "90": "Ninety",
}

DIVIDER_WORD_LIST = ["", "Thousand", "Million", "Billion",
                     "Quadrillion", "Quitillion", "Sextillion"]

class IntegerConverter(object):
    """
    A converter that takes a number in str format and outputs its word representation.
    Supports non-negative integer upto 10^22 (exclusive) at the moment.
    """

    def input_validator(self, num_str):
        """
        Validate input number. Must be non-negative integer less than 10^22.
        """
        try:
            number = int(num_str)
            if number < 0 or number >= 10**22:
                raise ValueError
        except ValueError as err:
            return False

        return True

    def convert(self, num_str):
        """
        Convert a number string into its word representation.
        """
        num_str_list = self.split_str(num_str)
        num_of_chunks = len(num_str_list)

        result_str = ""

        for i in range(num_of_chunks):
            sub_str = self.three_digits_converter(num_str_list[i])
            if sub_str:
                result_str += sub_str + " " + DIVIDER_WORD_LIST[num_of_chunks-1-i] + " "

        if not result_str:
            result_str = "Zero"

        return result_str.strip()

    def append_zero(self, num_str, n=3):
        """
        Append a num_str with zeros at the front to make its number of digits
        divisible by n.
        """
        appended_str = "0" * (n - len(num_str) % n) + num_str

        return appended_str

    def split_str(self, num_str, n=3):
        """
        Evenly split a number string every n digits, return results as a list.
        """
        num_str = self.append_zero(num_str, n)
        splitted_str = [num_str[i:i+n] for i in range(0, len(num_str), n)]

        return splitted_str

    def three_digits_converter(self, num_str):
        """
        Convert a thre-digit number string into its word representation.
        """
        hundredth_digit = num_str[0]
        tenth_digit = num_str[1]
        decimal_digit = num_str[2]

        result_str = ""

        if hundredth_digit != "0":
            result_str += NUM_LOOKUP_TABLE[hundredth_digit] + " Hundred "

        if tenth_digit == "1":
            result_str += NUM_LOOKUP_TABLE[tenth_digit + decimal_digit]
        else:
            if tenth_digit != "0":
                result_str += NUM_LOOKUP_TABLE[tenth_digit + "0"]
                if decimal_digit != "0":
                    result_str += "-"
            if decimal_digit != "0":
                result_str += NUM_LOOKUP_TABLE[decimal_digit]

        return result_str.strip()