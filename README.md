# IntegerConverter

A web service that converts a non-negative integer to its English words equivalent on request.
Currently supporting integer upto (not including) 10^22.

### How to use:
1. Build the Docker image locally using the Dockerfile provided in the project by running:
   ```
   docker build -t chelseylin1992/integer-converter:1.0 .
   ```
   Or pull the image directly from following repository: [chelseylin1992/integer-converter](https://hub.docker.com/r/chelseylin1992/integer-converter):
   ```
   docker pull chelseylin1992/integer-converter:1.0
   ```
2. Start a container using the image:
   ```
   docker run -p 8080:8080 -t chelseylin1992/integer-converter:1.0
   ```
3. Use the converter by sending a `GET` request, with an integer passed in as parameter `n`. One way is to use the `curl` command with `-d` or simply as part of the url:
   ```
   curl http://localhost:8080?n=123
   curl -G http://localhost:8080 -d "n=123"
   
   # EXPECTED RETURN:  
   One Hundred Twenty-Three
   ```

### Reference:
* [Cherrypy official documentation](https://docs.cherrypy.org/en/latest/index.html)
* [Stack Overflow - how to split string evenly](https://stackoverflow.com/questions/9475241/split-string-every-nth-character)