# Dockerfile for Integer Converter Service
#
# Chelsey Lin

FROM alpine:3.7 

RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev && \
    apk add --no-cache --update python3 && \
    pip3 install --upgrade pip setuptools
RUN pip3 install cherrypy

COPY converter_ws.py /root/converter/converter_ws.py
COPY integer_converter.py /root/converter/integer_converter.py

RUN chmod +x /root/converter/converter_ws.py

WORKDIR /root/converter
EXPOSE 8080
CMD python3 "./converter_ws.py"
